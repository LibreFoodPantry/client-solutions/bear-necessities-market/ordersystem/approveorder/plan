# OrderModule-bnm/PlaceOrder/Plan

Developers use this project to plan, learn, share, and design for this module.


## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/OrderModule-bnm/PlaceOrder/Plan)
- [Edit on GitLab](docs) (in `docs/` directory)
- [Requirements](ApproveOrder_Requirement.md)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
