# Title: Approve orders to be filled

## Pre-Checklist
I have searched for, and did not find, an existing issue or other requirement at the top-level of the LibreFoodPantry project.

## Stories
Story 1: As an Administrator, I want to send approved orders to FillOrder, so that the Guest may have their order filled.

## Ready Checklist
|Story 1: As an Administrator, I want to send approved orders to FillOrder, so that the Guest may have their order filled.           |       |
| --------------------------------------------------------------------------------------------------------------------------- |:----:|
| Independent of other issues being worked on                                                                                 | Yes  |
| Negotiable (and negotiated)                                                                                                 | Yes  |
| Valuable (the value to an identified role has been identified)                                                              | Yes  |
| Estimable (the size of this story has been estimated)                                                                       | No   |
| Small (can be completed in 50% or less of a single iteration)                                                               | No   |
| Testable (has testable acceptance criteria)                                                                                 | Yes  |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | No   |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | No   |

## Diagrams

Activity 1.0:

A Guest submits an order. The Administrator receives a notification that they have a new order to review. If the order fulfills all requirements the Administrator approves the order. The approved order is sent off to get filled.

Diagram 1.1:

![](diagrams/Diagram2.0.png)

## Acceptance Criteria

### Scenario: An Order is cccepted by an Administrator<br/>
Given:    An Administrator approves an order<br/>

When:     An order is approved<br/>

Then:     Send order to FillOrder<br/>

And:      Send Guest a confirmation that the order was approved<br/>


## Related Issues
Parent: [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/25](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/25)

## Estimate
This requirement would be a weight of **5** ; a full sprint would be needed to complete this. In this requirement, we might need to communicate with the FillOrder Team on how they are set up and how they want to receive the approved orders.

<br/>

# Title: Guest receives status notification of their order

## Pre-Checklist
It was expressed by the Administration for BNM that they would like the guest to receive confirmation and notification if their orders were received and approved or rejected.

## Stories
Story 1:
As a Guest, I want a confirmation that an order is approved, so that I know that my order is being processed.

Story 2:
As a Guest, I want a notification if an order is rejected, so that I know why my order was not processed.

## Ready Checklist
|Story 1: As a Guest, I want a confirmation if an order is approved, so that I know that my order is being processed.                |       |
| --------------------------------------------------------------------------------------------------------------------------- |:----:|
| Independent of other issues being worked on                                                                                 | Yes  |
| Negotiable (and negotiated)                                                                                                 | Yes  |
| Valuable (the value to an identified role has been identified)                                                              | Yes  |
| Estimable (the size of this story has been estimated)                                                                       | No   |
| Small (can be completed in 50% or less of a single iteration)                                                               | No   |
| Testable (has testable acceptance criteria)                                                                                 | Yes  |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | No   |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | No   |

|Story 2: As a Guest, I want a notification if an order is rejected, so that I know why my order was not processed.                  |       |
| --------------------------------------------------------------------------------------------------------------------------- |:----:|
| Independent of other issues being worked on                                                                                 | Yes  |
| Negotiable (and negotiated)                                                                                                 | Yes  |
| Valuable (the value to an identified role has been identified)                                                              | Yes  |
| Estimable (the size of this story has been estimated)                                                                       | No   |
| Small (can be completed in 50% or less of a single iteration)                                                               | No   |
| Testable (has testable acceptance criteria)                                                                                 | Yes  |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | No   |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | No   |


## Diagrams and Activities

Activity 2.0:

Guest submits an order. Administrator validates if order fulfills requirements to be approved. Once the Administrator approves the order, the Guest receives a confirmation email that their order has been approved.

Activity 2.1:

A Guest submits an order. The Administrator gets a notification that they have a new order to review. The Administrator must validate if the order fulfills all requirements before it can be approved. If the order fails any requirements then the Administrator rejects the order. Once the order is rejected an email notification is sent to the Guest who has placed the order, notifying them that their order has been rejected, including the reason for its rejection.

Diagram 2.3:

![](diagrams/Diagram2.0.png)

## Acceptance Criteria

### Scenario: Verifying Guest has the ability to receive an order<br/>
Given:    An order request is made<br/>

When:     An order is received<br/>

Then:     An Administrator should verify if the Guest is a current student<br/>

And:      Has not received more than one order in a weekly period<br/>

### Scenario: Order is approved<br/>
Given:    An order is approved<br/>

When:     An order fulfills all validation requirements<br/>

Then:     The order will be filled<br/>

And:      The Guest receives a confirmation notification that their order will be filled<br/>

### Scenario: Order is not approved<br/>
Given:    An order is rejected<br/>

When:     An order is not approved<br/>

Then:     Send the Guest an notification that their order has been rejected based on failing criteria<br/>

And:      Ask the Guest to resubmit order<br/>

## Related Issues
Parent: [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/27](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/27)


## Estimate
Story 1: As a Guest, I want a confirmation that the order is approved, so that I know that my order is being processed.
This requirement would be a weight of **3** ; it would be more than a day&#39;s worth of work but less than a full sprint. This might depend on how the guest is being contacted (text or email) and the testing of this feature.

Story 2: As a Guest, I want a notification if an order is rejected, so that I know why my order was not processed.
This requirement would be a weight of **3** ; it would be more than a day&#39;s worth of work but less than a full sprint. This might depend on how the guest is being contacted (text or email) and the testing of this feature.

<br/>

# Title: Administrator approves order

## Pre-Checklist
I have searched for, and did not find, an existing issue or other requirement at the top-level of the LibreFoodPantry project.

## Stories
Story 1: As an Administrator, I want to validate/approve the order, so that the Guest's order may be filled.

## Ready Checklist
|Story 1: As an Administrator, I want to validate/approve the order, so that the Guest's order may be filled.                        |       |
| --------------------------------------------------------------------------------------------------------------------------- |:----:|
| Independent of other issues being worked on                                                                                 | Yes  |
| Negotiable (and negotiated)                                                                                                 | Yes  |
| Valuable (the value to an identified role has been identified)                                                              | Yes  |
| Estimable (the size of this story has been estimated)                                                                       | No   |
| Small (can be completed in 50% or less of a single iteration)                                                               | No   |
| Testable (has testable acceptance criteria)                                                                                 | Yes  |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | No   |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | No   |

## Diagrams

Activity 3.0:

A Guest submits an order. The Administrator gets a notification that they have a new order to review. The Administrator must validate if the order fulfills all requirements before it can be approved. To reach approval, the Administrator must be able to confirm that the Guest (who placed the order) has an active WNEU email, if their previous order matches the BNM pantry&#39;s history, and if an Administrator or Staff is able to confirm the Guest&#39;s identity from their last in-person visit. Once all these requirements are met, the Administrator approves the order.

Diagram 3.1:

![](diagrams/Diagram3.0.png)

![](diagrams/Diagram4.0.png)

## Acceptance Criteria

### Scenario: Accepting an order submitted by Guest<br/>
Given:    A Guest submits an order<br/>

When:     An order is received<br/>

Then:     Send order to FillOrder<br/>

And:      Send Guest a confirmation that the order was approved<br/>


## Related Issues
Parent: [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/27](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/27) [https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/26](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/approveorder/plan/-/issues/26)

## Estimate
This requirement would be a weight of **3** ; it would be more than a day&#39;s worth of work but less than a full sprint.

<br/>